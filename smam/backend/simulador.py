#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: simulador.py
# Capitulo: 3 Patrón Publica-Subscribe
# Autor(es): Perla Velasco & Yonathan Mtz.
# Version: 2.0.1 Mayo 2017
# Descripción:
#
#   Esta clase define el rol de un set-up, es decir, simular el funcionamiento de los wearables del caso de estudio.
#
#   Las características de ésta clase son las siguientes:
#
#                                          simulador.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |  - Iniciar el entorno   |  - Define el id inicial|
#           |        set-up         |    de simulación.       |    a partir del cuál se|
#           |                       |                         |    iniciarán los weara-|
#           |                       |                         |    bles.               |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +-------------------------+--------------------------+-----------------------+
#           |         Nombre          |        Parámetros        |        Función        |
#           +-------------------------+--------------------------+-----------------------+
#           |                         |                          |  - Inicializa los     |
#           |                         |                          |    publicadores       |
#           |     set_up_sensors()    |          Ninguno         |    necesarios para co-|
#           |                         |                          |    menzar la simula-  |
#           |                         |                          |    ción.              |
#           +-------------------------+--------------------------+-----------------------+
#           |                         |                          |  - Ejecuta el método  |
#           |                         |                          |    publish de cada    |
#           |     start_sensors()     |          Ninguno         |    sensor para publi- |
#           |                         |                          |    car los signos vi- |
#           |                         |                          |    tales.             |
#           +-------------------------+--------------------------+-----------------------+
#
#-------------------------------------------------------------------------
import sys
sys.path.append('publicadores')
from xiaomi_my_band import XiaomiMyBand

from threading import Thread, activeCount
import time


class Simulador(Thread):

    def __init__(self, id):
        super(Simulador, self).__init__()
        self.band = XiaomiMyBand(id)
        self.__stopped = False

    def run(self):
        remaining_messages = 50
        while(remaining_messages and not self.__stopped):
            self.band.publish()
            remaining_messages-= 1

        self.__stopped = True


    def stop(self):
        self.__stopped = True




if __name__ == '__main__':
    sensores = []
    id_inicial = 39722608

    print('+---------------------------------------------+')
    print('|  Bienvenido al Simulador Publica-Subscribe  |')
    print('+---------------------------------------------+')
    print('')
    raw_input('presiona enter para continuar: ')
    print('+---------------------------------------------+')
    print('|        CONFIGURACIÓN DE LA SIMULACIÓN       |')
    print('+---------------------------------------------+')
    adultos_mayores = raw_input('|        número de adultos mayores: ')
    print('+---------------------------------------------+')
    raw_input('presiona enter para continuar: ')
    print('+---------------------------------------------+')
    print('|            ASIGNACIÓN DE SENSORES           |')
    print('+---------------------------------------------+')
    for x in xrange(0, int(adultos_mayores)):
        s = Simulador(id_inicial)
        #s.setDaemon(True)
        sensores.append(s)
        print('| wearable Xiaomi My Band asignado, id: ' + str(id_inicial))
        print('+---------------------------------------------+')
        id_inicial += 1
    print('+---------------------------------------------+')
    print('|        LISTO PARA INICIAR SIMULACIÓN        |')
    print('+---------------------------------------------+')
    raw_input('presiona enter para iniciar: ')

    for s in sensores:
        s.start()
        
    print("###################################################simulación terminada.###########################################################")


